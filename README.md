# Image Convolution by Dask Clusler

[[_TOC_]]

## Description

Two simple jupyter notebooks, where a set of fit images are convolved with a kernel.

1. conv_fitsimages_multiprocessing.ipynb
   It uses the multiprocessing python module to parallize convolution: 
   - conv_fitsimages_multiprocessing.ipynb


2. conv_fitsimages_multiprocessing.ipynb
It computes the convolution through a clutster through th Dask:
   - conv_fitsimages_dash.ipynb


