#!/usr/bin/env python3
#
# -*- coding: utf-8 -*-

"""
October 2023
M. A. Mendoza
"""

import os
import argparse
import logging
import time
import numpy
import multiprocessing
from glob import glob
import scipy
from astropy.io import fits



##----------------------------------------------------------------------------
class NoDaemonProcess(multiprocessing.Process):
    @property
    def daemon(self):
        return False

    @daemon.setter
    def daemon(self, value):
        pass


class NoDaemonContext(type(multiprocessing.get_context())):
    Process = NoDaemonProcess

# We sub-class multiprocessing.pool.Pool instead of multiprocessing.Pool
# because the latter is only a wrapper function, not a proper class.
class MyPool(multiprocessing.pool.Pool):
    def __init__(self, *args, **kwargs):
        kwargs['context'] = NoDaemonContext()
        super(MyPool, self).__init__(*args, **kwargs)


def parallelize(func, l_iter, max_jobs=1, args=(), kwargs={}):

    ## Parallel processing
    if (max_jobs > 1) and len(l_iter) > 0:
        pool = MyPool(max_jobs)
        [pool.apply_async(func, (item,) + args, kwargs)
                   for item in l_iter]
        pool.close()
        pool.join()
    ## Serial processing
    else:
        for item in l_iter:
            func(item, *args, **kwargs)


## ----------------------------------------------------------------------------
def convolve_image(f_image, dirOUT='.', prefix='.'): 
    
    #Load the image
    hdu = fits.open(f_image)
    image = hdu[0].data    

    # Convolution
    w = numpy.ones(image.ndim * (3,), dtype=numpy.float32)
    image_conv = scipy.ndimage.convolve(image, w)
    
    #save image
    if not os.path.exists(dirOUT):
        logger.info('mkdir %s' % dirOUT)
        os.makedirs(dirOUT)
        
    hdu[0].data = image_conv
    nameOut= '%s%s' % (prefix, os.path.basename(f_image))
    f_out = os.path.join(dirOUT, nameOut)
    hdu.writeto(f_out, overwrite=True)
    
    return image_conv


### logging
logger = logging.getLogger('image_processing_log')
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(message)s', datefmt='%H:%M:%S')
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
ch.setFormatter(formatter)
logger.addHandler(ch)

## main
if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='image processing')
    parser.add_argument('--ncpus', type=int, default=1,
                         help='Number of cpus to use (default:1))')
    parser.add_argument('--dirIN', type=str, default='.',
                        help='dir where the images lay')
    parser.add_argument('--dirOUT', type=str, default='.',
                        help="dir where the convolved images are saved")
    parser.add_argument('--pattern', type=str, default='*.fits', 
                        help = 'name pattern of the images to process ')
    parser.add_argument('--prefix', type=str, default='',
                        help='name prefix of the convolved images')
 
    args = parser.parse_args()
    
    ncpus = args.ncpus
    dirIN = args.dirIN
    dirOUT = args.dirOUT
    pattern = args.pattern
    prefix = args.prefix
    
    # load images
    limages = glob(os.path.join(dirIN, pattern))

    # Convolve in parallel
    logger.info("Convolve %i images" % len(limages))
    max_jobs=ncpus
    args =()
    kwargs = {'dirOUT': dirOUT, 'prefix': prefix}
    t = time.time()
    parallelize(convolve_image, limages, max_jobs, args, kwargs)
    elapsed_time = time.time() - t
    logger.info("DONE in %f ms" % (elapsed_time*1000))
