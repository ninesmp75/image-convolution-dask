#!/usr/bin/env python
#
# -*- coding: utf-8 -*-

"""
September 2023
M. A. Mendoza
"""


from dask_gateway import Gateway, GatewayCluster

import dask_image.imread
import dask_image.ndfilters
import dask_image.ndmeasure

import numpy
from skimage import data, io

import matplotlib.pyplot as plt


# Funtion for image convolving with a kernel (w)
def convolve_image(image, w):    
    data = dask_image.ndfilters.convolve( image, w)
    return data


## main
if __name__ == '__main__':

    ## Connection to Dask Cluster and Set-up
    gateway = Gateway()
    cluster = GatewayCluster()
    gateway.list_clusters()
  
    # Scale the cluster: Change this parameter according to your preferences.
    # For example cluster.scale(2) is a 2 nodes cluster with 4 GB each one.
    cluster.scale(2)
    ## Show the cluster environment
    cluster
 
    client = cluster.get_client()
    ## See the current status of the client
    client

    #Download and save a public image
    io.imsave('astronaut.png', data.astronaut())
    #Read image
    image = dask_image.imread.imread('astronaut.png')

    # convolve image
    w = numpy.ones(image.ndim * (3,), dtype=numpy.float32)
    convolved_image = convolve_image(image, w)
    convolved_image = convolved_image.compute()

    # Show images
    fig, (ax0, ax1) = plt.subplots(nrows=1, ncols=2)

    # Subplot headings
    ax0.set_title('Original image')
    ax1.set_title('Convolved image')

    # Don't display axes
    ax0.axis('off')
    ax1.axis('off')

    # Display images
    ax0.imshow(image[0, ...])
    ax1.imshow(convolved_image[0, ...])

    # IMPORTANT - Close the Dask client and cluster
    client.close()
    cluster.close() 
    cluster.shutdown()
