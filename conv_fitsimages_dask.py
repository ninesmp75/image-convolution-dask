#!/usr/bin/env python3
#
# -*- coding: utf-8 -*-

"""
October 2023
M. A. Mendoza
"""

import os
import argparse
from glob import glob

from dask_gateway import Gateway, GatewayCluster

import dask
import dask.array as da
from astropy.io import fits

import numpy
import scipy
import logging
import time

### logging
logger = logging.getLogger('image_processing_log')
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(message)s', datefmt='%H:%M:%S')
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
ch.setFormatter(formatter)
logger.addHandler(ch)

@dask.delayed
def convolve_image(f_image, dirOUT='.', prefix='.'): 
    
    #Load the image
    hdu = fits.open(f_image)
    image = hdu[0].data    

    # Convolution
    w = numpy.ones(image.ndim * (3,), dtype=numpy.float32)
    image_conv = scipy.ndimage.convolve(image, w)
    
    #save image
    if not os.path.exists(dirOUT):
        logger.info('mkdir %s' % dirOUT)
        os.makedirs(dirOUT)
        
    hdu[0].data = image_conv
    nameOut= '%s%s' % (prefix, os.path.basename(f_image))
    f_out = os.path.join(dirOUT, nameOut)
    hdu.writeto(f_out, overwrite=True)
    
    return image_conv


## main
if __name__ == '__main__':
    
    parser = argparse.ArgumentParser(description='image processing')
    parser.add_argument('--ncpus', type=int, default=1,
                         help='Number of cores to use (default:1))')
    parser.add_argument('--dirIN', type=str, default='.',
                        help='dir where the images lay')
    parser.add_argument('--dirOUT', type=str, default='.',
                        help="dir where the convolved images are saved")
    parser.add_argument('--pattern', type=str, default='*.fits', 
                        help = 'name pattern of the images to process ')
    parser.add_argument('--prefix', type=str, default='',
                        help='name prefix of the convolved images')
 
    args = parser.parse_args()
    
    ncores = args.ncores
    dirIN = args.dirIN
    dirOUT = args.dirOUT
    pattern = args.pattern
    prefix = args.prefix

    ## Connection to Dask Cluster and Set-up
    gateway = Gateway()
    cluster = GatewayCluster()
    gateway.list_clusters()
  
    # Scale the cluster: Change this parameter according to your preferences.
    # For example cluster.scale(2) is a 2 nodes cluster with 4 GB each one.
    cluster.scale(ncores)
    ## Show the cluster environment
    cluster
 
    client = cluster.get_client()
    ## See the current status of the client
    client

    # Path of the images
    limages = glob(os.path.join(dirIN, pattern))
    
    
    # convolve image
    logger.info("Convolve %i images" % len(limages))
    t = time.time()
    lazy_images = [convolve_image(file, dirOUT, prefix) for file in limages]
    conv_images = [da.from_delayed(lazy_image,         
                              dtype=lazy_image.compute().dtype,   
                              shape=lazy_image.compute().shape)
                   for lazy_image in lazy_images]
    elapsed_time = time.time() - t
    logger.info("DONE in %f ms" % (elapsed_time*1000))


    # IMPORTANT - Close the Dask client and cluster
    client.close()
    cluster.close() 
    cluster.shutdown()
